﻿using UnityEditor;
using System.IO;

public class CreateAssetBundles
{
    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        BuildTarget activeBuildTarget = EditorUserBuildSettings.activeBuildTarget;

        string assetBundleDirectory = "Assets/AssetBundles/" + activeBuildTarget.ToString();
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }
        BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, activeBuildTarget);
    }
}