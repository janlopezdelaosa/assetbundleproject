﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AssetDownloader : MonoBehaviour
{
    public string url;
    GameObject waterCraftGO;
    public Text loadingText;
    public Transform spawnPos;

    IEnumerator LoadBundle(string watercraftName)
    {
        while (!Caching.ready)
        {
            yield return null;
        }

        string platform = Application.platform.ToString();
        
        string finalurl = url + "/" + platform + "/" + watercraftName;
        print("checking in " + finalurl);

        //Begin download
        WWW www = WWW.LoadFromCacheOrDownload(finalurl, 0);
        yield return www;

        //Load the downloaded bundle
        AssetBundle bundle = www.assetBundle;
        foreach (string s in bundle.GetAllAssetNames()) print(s);

        //Load an asset from the loaded bundle
        AssetBundleRequest bundleRequest = bundle.LoadAssetAsync(watercraftName, typeof(GameObject));
        yield return bundleRequest;
        //get object
        GameObject obj = bundleRequest.asset as GameObject;
        if (obj == null) print("oBjeteeee");
        //waterCraftGO = Instantiate(obj, spawnPos.position, Quaternion.identity) as GameObject;
        waterCraftGO = Instantiate(obj, spawnPos.position, Quaternion.identity) as GameObject;
        waterCraftGO.transform.SetParent(spawnPos);
        loadingText.text = "";

        bundle.Unload(false);
        www.Dispose();
    }

    public void Load(string watercraftName)
    {
        if (waterCraftGO)
        {
            Destroy(waterCraftGO);
        }

        if (Caching.ClearCache())
            print("Caché clear");
        else print("Caché couldn't be clear");

        loadingText.text = "Loading " + watercraftName + "...";
        StartCoroutine(LoadBundle(watercraftName));
    }
}